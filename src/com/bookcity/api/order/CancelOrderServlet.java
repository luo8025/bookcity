package com.bookcity.api.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.OrderServiceImpl;
import com.google.gson.Gson;



/**
 * Servlet implementation class newdemo
 */
@WebServlet("/api/order/CancelOrderServlet")
public class CancelOrderServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderUserId=request.getParameter("userId");
		String orderId=request.getParameter("orderId");
		JsonResult result=null;
		try {
			OrderServiceImpl orderserviceimpl=new OrderServiceImpl();
			orderserviceimpl.cancelorder(orderId, orderUserId);
			try {
				result=new JsonResult(Constants.STATUS_SUCCESS, "取消成功" );
			}catch(Exception esssx) {
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "用户名或订单号出错");
			}
		}catch(Exception ex){
			result=new JsonResult(Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}
}