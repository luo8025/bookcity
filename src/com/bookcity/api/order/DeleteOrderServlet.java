package com.bookcity.api.order;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.OrderServiceImpl;
import com.google.gson.Gson;




/**
 * Servlet implementation class reg
 */
@WebServlet("/api/order/DeleteOrderServlet")
public class DeleteOrderServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				String orderId=request.getParameter("orderId");
				
				JsonResult result=null;
				try {
					OrderServiceImpl orderserviceimpl=new OrderServiceImpl();
					
					if(!"".equals(orderId)&&orderId!=null) {
						orderserviceimpl.deleteorder(orderId);
						result=new JsonResult(Constants.STATUS_SUCCESS, "ɾ���ɹ�",orderId);
					}else {
						result=new JsonResult(Constants.STATUS_NOT_FOUND,"ɾ��ʧ��");
					}
				}
				catch(Exception ex) {
					result=new JsonResult(Constants.STATUS_FAIL,"ɾ���쳣",ex.getMessage());
				}
			
				
				JsonResultWriter.writer(response, result);
			}
		
	}