package com.bookcity.api.order;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Order;
import com.bookcity.model.ShopCart;
import com.bookcity.service.OrderServiceImpl;


/**
 * Servlet implementation class OrderSettlementServlet
 */
@WebServlet("/api/order/OrderSettlementServlet")
public class OrderSettlementServlet extends HttpServlet {
	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//2)获得数据
		
		String orderUserId=request.getParameter("orderUserId");
		String buyUserName=request.getParameter("buyUserName");
		String orderTotal=request.getParameter("orderTotal");
		String orderBookId=request.getParameter("orderBookId");
		String shopCartId=request.getParameter("shopCartId");
		
		
		//3)处理业务逻辑
		//String sql="INSERT into `user` VALUES (UUID(),?,?,?,?,?,?,?) ";
		
		//创建一个HashMap封装消息 HashMap转换成消息后封装成对象{}  ArrayList转换成json后变成一个数组
		JsonResult result=null;
		try {
			OrderServiceImpl impl=new OrderServiceImpl();
			Order o=new Order();
			ShopCart cart=new ShopCart();
			o.setOrderUserId(orderUserId);
			o.setOrderUserName(buyUserName);
			o.setOrderTotal(orderTotal);
			o.setOrderSendPlace("");
			o.setOrderBookId(orderBookId);
			cart.setShopcart_id(shopCartId);
			o.setShopCart(cart);
			if(!"".equals(o.getOrderid())&&orderUserId!=null){
				impl.settlement(o);
				result=new JsonResult(Constants.STATUS_SUCCESS, "结算成功",o);
			
			} else  {
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "结算失败");
				
			}
		} catch (Exception e) {
		
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}
}
