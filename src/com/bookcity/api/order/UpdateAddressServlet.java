package com.bookcity.api.order;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Order;
import com.bookcity.service.OrderServiceImpl;

/**
 * Servlet implementation class UpdateAddressServlet
 */
@WebServlet("/api/order/UpdateAddressServlet")
public class UpdateAddressServlet extends HttpServlet {
	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//2)获得数据
		
		String address=request.getParameter("address");
		String id=request.getParameter("id");

		
		//3)处理业务逻辑
		//String sql="INSERT into `user` VALUES (UUID(),?,?,?,?,?,?,?) ";
		
		//创建一个HashMap封装消息 HashMap转换成消息后封装成对象{}  ArrayList转换成json后变成一个数组
		JsonResult result=null;
		try {
			OrderServiceImpl impl=new OrderServiceImpl();
			int i=impl.insertAddress(address, id);
			
			if(i>0){
				result=new JsonResult(Constants.STATUS_SUCCESS, "插入成功",address);
			
			} else  {
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "插入失败");
				
			}
		} catch (Exception e) {
		
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}
}
