package com.bookcity.api.uesr;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.BookServiceImpl;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class UpUserheadportraituserServlet
 */
@WebServlet("/UpUserheadportraituserServlet")
public class UpUserheadportraituserServlet extends HttpServlet {
	public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String photo =request.getParameter("photo");
		String userid=request.getParameter("userid");
	
		JsonResult result=null;
		try {
			UserServiceImpl impl=new UserServiceImpl();
			impl.headportraituser(photo, userid);
			if(photo!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS,"修改成功");
				}
			else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"修改失败","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"修改异常",ex.getMessage());
		}

		JsonResultWriter.writer(response,result);
	

	}
		

}
