package com.bookcity.api.uesr;



import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bookcity.common.Config;
import com.bookcity.common.Constants;
import com.bookcity.common.HttpUtil;


/**
 * 获取验证码短信验证码接口
 */

@WebServlet("/api/smscheck")
public class SMSCheckServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//生成6位验证码
		int code=(int)(Math.random()*(9999-1000+1))+100000;
		HttpSession session = request.getSession();
	    // 将认证码存入SESSION
	    session.setAttribute("code", code);
	    System.out.println(code);
		String smsContent ="【同城二手书交易】您的验证码为"+code+"，请于2分钟内正确输入，如非本人操作，请忽略此短信。";
		String to = request.getParameter("telphone");
		String tmpSmsContent = null;
	    try{
	      tmpSmsContent = URLEncoder.encode(smsContent, "UTF-8");
	    }catch(Exception e){
	    	System.out.println(e.getMessage());
	    }
	    String url = Config.BASE_URL + Constants.operation;
	    String body = "accountSid=" + Constants.accountSid + "&to=" + to + "&smsContent=" + tmpSmsContent + HttpUtil.createCommonParam();

	    // 提交请求
	    String result = HttpUtil.post(url, body);
	    System.out.println("result:" + System.lineSeparator() + result);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}
}