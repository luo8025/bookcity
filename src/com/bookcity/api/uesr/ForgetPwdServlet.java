package com.bookcity.api.uesr;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.User;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class ForgetPwdServlet
 */
@WebServlet("/api/findPwd/ForgetPwdServlet")
public class ForgetPwdServlet extends HttpServlet {
	/**
	 * request 请求对象
	 * response 响应对象 
	 */
	@Override
	public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String userName=request.getParameter("userName");
		String verification=request.getParameter("verification");
		
		verification=verification.toLowerCase();
		String obj=(String) request.getSession().getAttribute("code");
		obj=obj.toLowerCase();
		JsonResult result=null;
		try {
			
			if(obj.equals(verification)){
				UserServiceImpl impl=new UserServiceImpl();
				List<HashMap<String,Object>> list=impl.forgetPwd(userName);
				
				if(list.size()>0){
					result=new JsonResult(Constants.STATUS_SUCCESS, "验证成功",list);
				}else{
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "用户名不存在");
				}
			}else{
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "验证码错误");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
		
		
	}


}
