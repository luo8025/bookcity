package com.bookcity.api.uesr;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bookcity.common.Config;
import com.bookcity.common.Constants;
import com.bookcity.common.HttpUtil;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.User;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class ForgetPwdServlet2
 */
@WebServlet("/api/findPwd/ForgetPwdServlet2")
public class ForgetPwd2Servlet extends HttpServlet {
	/**
	 * request 请求对象
	 * response 响应对象 
	 */
	@Override
	public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String verification =request.getParameter("verification");
		String userName=request.getParameter("userName");
		String telphone=request.getParameter("telphone");
		JsonResult result=null;
		try {
			Integer obj=(Integer) request.getSession().getAttribute("code");
			String s=null;
			s=s.valueOf(obj);
			if(s.equals(verification)){
				UserServiceImpl impl=new UserServiceImpl();
				List<HashMap<String,Object>> list=impl.forgetPwd2(userName,telphone);
				if(list.size()>0){
					result=new JsonResult(Constants.STATUS_SUCCESS, "验证成功",list);
					
				}else{
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "验证失败");
				}
			}else{
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "验证码错误");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
		
		
	}

}
