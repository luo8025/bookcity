package com.bookcity.api.uesr;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bookcity.common.Config;
import com.bookcity.common.Constants;
import com.bookcity.common.HttpUtil;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.User;
import com.bookcity.service.LoginServiceImpl;




/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/api/user/login")
public class LoginUserServlet extends HttpServlet {
	/**
	 * request 请求对象
	 * response 响应对象 
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		String em = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		String ph = "^[1][34578]\\d{9}$";
		JsonResult result=null;
		try {
			
			LoginServiceImpl impl=new LoginServiceImpl();
			User user=new User();
			if(name.matches(em)){
				user=impl.loginUser(null,null,name,password);
			}else if(name.matches(ph)){
				user=impl.loginUser(null,name,null,password);
			}else{
				user=impl.loginUser(name,null,null,password);
				
			}
			if(user!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS, "登录成功", user);
				
			}else{
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "用户名或密码错误");
			}
			
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
		response.setHeader("Access-Control-Allow-Origin",request.getHeader("Origin") );
	    response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	    response.setHeader("Access-Control-Max-Age", "3600");
	    response.setHeader("Access-Control-Allow-Headers", "x-requested-with,Authorization");
	    response.setHeader("Access-Control-Allow-Credentials", "true");
		
	}
}
	
