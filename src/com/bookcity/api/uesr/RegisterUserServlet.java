package com.bookcity.api.uesr;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.User;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class RegisterUserServlet
 */
@WebServlet("/api/user/RegisterUserServlet")
public class RegisterUserServlet extends HttpServlet {
	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//2)�������
		String userName=request.getParameter("userName");
		String userPassword=request.getParameter("userPassword");
		String userTelphone=request.getParameter("userTelphone");
		String userEmail=request.getParameter("userEmail");
		String userSex=request.getParameter("userSex");
		String verification =request.getParameter("verification");
	    String photo="ͷ��.jpg";
		JsonResult result=null;
		try {
			Integer obj=(Integer) request.getSession().getAttribute("code");
			String s=null;
			s=s.valueOf(obj);
			if(s.equals(verification)){
				UserServiceImpl impl=new UserServiceImpl();
				User user=new User();
				UUID uuid=UUID.randomUUID();
				user.setUserId(uuid.toString());
				user.setUserName(userName);
				user.setUserPassword(userPassword);
				user.setUserTelphone(userTelphone);
				user.setUserEmail(userEmail);
				user.setUserSex(userSex);
				user.setUserPhoto(photo);
				/*try {
					impl.userRegister(user);
					result=new JsonResult(Constants.STATUS_SUCCESS, "ע��ɹ�",user);
				} catch (Exception e) {
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "ע��ʧ��");
				}*/
				int i=impl.userRegister(user);
				if(i>0){
					result=new JsonResult(Constants.STATUS_SUCCESS, "ע��ɹ�",user);
				
				} else  {
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "ע��ʧ��");
					
				}
			}else{
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "��֤�����");
			}
		} catch (Exception e) {
		
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "�������",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}

}
