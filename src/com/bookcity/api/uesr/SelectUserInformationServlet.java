package com.bookcity.api.uesr;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bookcity.common.Config;
import com.bookcity.common.Constants;
import com.bookcity.common.HttpUtil;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.User;
import com.bookcity.service.LoginServiceImpl;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class SelectUserInformationServlet
 */
@WebServlet("/api/user/SelectUserInformationServlet")
public class SelectUserInformationServlet extends HttpServlet {
	/**
	 * request 请求对象
	 * response 响应对象 
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String userId=request.getParameter("userId");
		JsonResult result=null;
		try {
			
				UserServiceImpl impl=new UserServiceImpl();
				User user=impl.selectUserInformation(userId);
				if(user!=null){
					result=new JsonResult(Constants.STATUS_SUCCESS, "查询成功", user);
					System.out.println(user.getUserpraise());
					System.out.println(user.getUserbad());
				}else{
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "查询失败");
				}
			
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
		
		
	}

}
