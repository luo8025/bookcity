﻿package com.bookcity.api.config;



import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	//1)设置中文编码
	

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016091800536616";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key ="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQClMP37RBO//OLkwB3SbU2pdhIeV4CP+K8U2mwNjz4dw0QJl4GkPvtFfbLMowk/XYDRsUwQzDARb8JULR+hcGhmYT8+t8ksosGk1bLfaQm6WyUPdsa4Cxxrfyytagl+L7VNpxhu8qppURhSW20XyDcIU1IkbG7hIwu7C8KKPokicJSNXRE+Z2ax6tWyiHGAHqMUFWYCEgIh8u3C7DxUL3tDqSFdLcTyaOvMCqsfkKIcNSULgJuD5MmMNSBX4AlQDpRMv2Gp/PdKZdiJHN/POihyn/cDzKWvZR996Ry6EBvV0HaaJWX86D7/mWpx66tyQMTbp6d/pIUlXgeMhQf9uFKRAgMBAAECggEAEfyjqxhejikbC41fsen06t7QWj1s4kBR0BN4cIGIYfVBE0pL83ZoJcjqDx4xg5TK0gpcHfgdw8hHgKLpLsrukzdDi7C3VwTh0PDz6R+mz4PdJTHpsWCEV7AhTUCeh/7p3uqy3mHN7GnIHC+YUV6bn2xzJUQpTppiTJubMzJb/EPYyipwXmCotu2+LIz0gvIAX/B0UeZczWvhCK67j6tvz+yA+qhH4eo5DJkCLAKHcLYX0YesJpnH4DVZfeWhl5jVoIxrnsdG8k3xxLZdfrL7SjFSoPT3C/jLq2zUtTe/YGBnk50V7n5LMeNRjKRPHTdCcLvcHtt4PTzgFb07yXFZgQKBgQDoeAH6fnc6KqUyiiGVd+mCfkBgLoVLc5ZxM86h5X0GXJBnHVou3CdndjkPG1qnfsePg33EjyJu/7RLkXPU+qusHyPlvTshrHqT+Z3wmnLvS2Xvt1Rq8Tj6QsdoTwDZtWufxJ+MXQ9OeKfA0SMGn+Pwt+rES5MxGGgF8aD41k2qOwKBgQC16Z3eLq58Cnv7wdUHFb/p/Iv5HTYWZt/8ZTkAqP4990ztptANHH6KZ4nnmm2IRkUZRpgpGUgl9RGz8O3x2ZqvBDTgi6YT1pGUP/5H/jtilPsA5yy/4AsnYCLZFIOdGYOMpSPt32ip63IdAG4bAlllpYVSv0Z0dKt7xXZfRfDdowKBgEdgIWthzQWk9lfQJYOY769LSES/w9lowfY76+O1bG+5l/SxBHGC0u5nVBmZUno+6NLdko35TJ88D4VsQV5RIMiWPxVYxoEOTgyFBcNMCKIESmYRqe+z5MSiIEuchixSEZtaaYpVQE/dp9QB4nk0Om48ae6fLbBTSA0bJUuBsRLlAoGAH4g/NSQiWp32n80ZgADlI0oDeUr29Ssf6EAjgVnJuDQFwawIpD0sMhGAdgTut5B2qcry1SzstKemSeqZVDLD/VJdWA7c2tN2qJvvF8uZv5zTZXSs7RWI2Wpwn/BOYAzsIJT/hVwUaF/Hh/9rFoNxu4zVb2J4c/dFQk8YFaup9MECgYEAi6FP3FsCirGrrifA8pFV7Xgs/DDpqgbLAIG0luoudT/QkeXT/7j2t5G7hOZVpZyNKD21LWotfBi9r3YRG8LEsSA+ShXvXKNXysLEhULcImaFi29ofDRzpD73Iy47LJIbLYK+A2qPKme/gRaqd620oYtLoLxxSCZnYtZWBJY++pM=";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0bdk+Tb+HM/PUfFnvUq5gff5JvW02sirNFFye0bOOYNyfo9/2uKfvfot+fEisHFrIRtUVitcL0hn46roxyhvxUuoXX1rPZcqpI5InrP3/B8tOuCDxfgWAEpyF+irVUh3H5E20xQNjqiL4xQQefic1dL0K0BDrOeaV0dR3j4iFC5cwiTlhkvwSgndbK9cTbj0kIB3+yjucyjLKJTwSDdwJQpZZ0jqAbX7lJGu/1qJNbfPnGsVgOIVo04yMTthI/sH/MW+IxnPySwGKoE5yoUchfZbbmjKKFllYopfcRjFs82PjaVCaj+SOihVLj6ijPgl9UFotQEsKLJuzDFog3CI2QIDAQAB";
	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/NotifyurlServlet";
  //	
	public static String return_url = "http://127.0.0.1:8020/BookCity/home.html";
	//public static String return_url = "http://127.0.0.1:8020/%E7%BD%91%E6%98%93%E4%B8%A5%E9%80%89%E6%9C%80%E7%BB%88%E7%89%88/user_index.html?__hbt=1534690923880";
	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

