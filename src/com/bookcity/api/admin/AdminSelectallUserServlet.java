package com.bookcity.api.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.common.PageModel;
import com.bookcity.service.AdminServiceImpl;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class AdminSelectallUserServlet
 * 管理员查看用户全部信息
 */
@WebServlet("/AdminSelectallUserServlet")
public class AdminSelectallUserServlet extends HttpServlet {
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pagesize=req.getParameter("pageSize");
        String page=req.getParameter("page");
        
        
		JsonResult<PageModel> result =null;
		try {
			
			AdminServiceImpl serivceimpl=new AdminServiceImpl();
			PageModel<List> model=serivceimpl.selectuser(page,pagesize);
		
			if(model!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",model);
				}
			else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"查询失败","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}

		JsonResultWriter.writer(resp,result);
	
}
}
