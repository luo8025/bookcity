package com.bookcity.api.admin;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.Out;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Admin;
import com.bookcity.service.AdminServiceImpl;
import com.google.gson.Gson;


/**
 * Servlet implementation class newdemo
 * @param <T>
 */
@WebServlet("/AdminLoginServlet")
public class AdminLoginServlet<T> extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String adminname=request.getParameter("username");
		String password=request.getParameter("password");
		String imgcodetext=request.getParameter("imgcodetext");
		JsonResult result=null;
		try {
		String text = (String) request.getSession().getAttribute("code");
		if (text.equals(imgcodetext)) {
			AdminServiceImpl adminservlet=new AdminServiceImpl();
			Admin admin=adminservlet.loginAdmin(adminname, password);
			if(admin!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS, "��½�ɹ�", admin);
			}else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "�˺��������");
			}
		}else {
			result=new JsonResult(Constants.STATUS_NOT_FOUND, "��֤�����");
		}
		}catch(Exception ex){
			result=new JsonResult(Constants.STATUS_FAIL,"��¼�쳣",ex.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

	


}