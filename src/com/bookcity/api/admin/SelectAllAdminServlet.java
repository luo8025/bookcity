package com.bookcity.api.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.AdminServiceImpl;
import com.bookcity.service.OrderServiceImpl;
import com.google.gson.Gson;



/**
 * Servlet implementation class newdemo
 */
@WebServlet("/api/order/SelectAllAdminServlet")
public class SelectAllAdminServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonResult result=null;
		try {
			AdminServiceImpl adminserviceimpl=new AdminServiceImpl();
			List<HashMap<String,Object>>list=adminserviceimpl.selectalladmin();
			if(list.size()>0) {
				result=new JsonResult(Constants.STATUS_SUCCESS, "查询成功",list );
			}else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "用户名出错");
			}
		}catch(Exception ex){
			result=new JsonResult(Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}
}