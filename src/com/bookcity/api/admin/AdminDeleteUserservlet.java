package com.bookcity.api.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.AdminServiceImpl;

/**
 * Servlet implementation class AdminDeleteUserservlet
 * ��idɾ���û�
 */
@WebServlet("/AdminDeleteUserservlet")
public class AdminDeleteUserservlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userid=req.getParameter("userid");
		JsonResult result=null;
		try {
			
			AdminServiceImpl serivceimpl=new AdminServiceImpl();
			serivceimpl.deleteuser(userid);
		
			if(userid!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS,"ɾ���ɹ�");
				}
			else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"ɾ��ʧ��","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"ɾ���쳣",ex.getMessage());
		}

		JsonResultWriter.writer(resp,result);
	

	}
}
