package com.bookcity.api.admin;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.User;
import com.bookcity.service.AdminServiceImpl;

/**
 * Servlet implementation class AdminSelectuserServlet
 */
@WebServlet("/AdminSelectSalesYearServlet")
public class AdminSelectSalesYearServlet extends HttpServlet {
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JsonResult result=null;
		try {
			
			AdminServiceImpl serivceimpl=new AdminServiceImpl();
			List<HashMap<String ,Object>> list=serivceimpl.selectSalesyear();
			if(list!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",list);
				}
			else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"查询失败","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}

		JsonResultWriter.writer(resp,result);
	}
}