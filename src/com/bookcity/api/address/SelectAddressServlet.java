package com.bookcity.api.address;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.common.PageModel;
import com.bookcity.service.AddressServiceImpl;
import com.bookcity.service.AdminServiceImpl;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class AdminSelectallUserServlet
 * 管理员查看用户全部信息
 */
@WebServlet("/api/address/SelectAddressServlet")
public class SelectAddressServlet extends HttpServlet {
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username=req.getParameter("userId");
		JsonResult result =null;
		try {
			
			AddressServiceImpl addressserviceimpl=new AddressServiceImpl();
			List<HashMap<String ,Object>> list=addressserviceimpl.selectaddressall(username);
		
			if(list!=null&&!list.equals("")){
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",list);
				}
			else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"查询失败");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}

		JsonResultWriter.writer(resp,result);
	
}
}