package com.bookcity.api.address;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Address;
import com.bookcity.model.User;
import com.bookcity.service.AddressServiceImpl;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class RegisterUserServlet
 */
@WebServlet("/api/address/DeleteAddressServlet")
public class DeleteAddressServlet extends HttpServlet {
	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//2)�������
		String addressuserid=request.getParameter("userId");
	
		JsonResult result=null;
		try {
				AddressServiceImpl impl=new AddressServiceImpl();
				int i=impl.delete(addressuserid);
				if(i>0){
					result=new JsonResult(Constants.STATUS_SUCCESS, "ɾ���ɹ�");
				
				} else  {
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "ɾ��ʧ��");
					
				}
		} catch (Exception e) {
		
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "�������",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}

}