package com.bookcity.api.shopcart;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Book;
import com.bookcity.model.ShopCart;
import com.bookcity.model.User;
import com.bookcity.service.ShopCartServiceImpl;
import com.bookcity.service.UserServiceImpl;

/**
 * Servlet implementation class AddShopCarServlet
 */
@WebServlet("/api/shopCart/AddShopCarServlet")
public class AddShopCarServlet extends HttpServlet {
	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//2)获得数据
		String shopcart_book_id=request.getParameter("shopcart_book_id");
		String shopcart_book_name=request.getParameter("shopcart_book_name");
		String shopcart_user_id=request.getParameter("shopcart_user_id");
		String shopcart_book_price=request.getParameter("shopcart_book_price");
		String shopcart_number=request.getParameter("shopcart_number");
		String shopcart_subtotal=request.getParameter("shopcart_subtotal");
		String userName=request.getParameter("userName");
		String bookPhoto=request.getParameter("bookPhoto");
		String bookdamage=request.getParameter("bookdamage");
		String bookStock=request.getParameter("bookStock");
		String bookremarks=request.getParameter("bookremarks");
		String bookUserId=request.getParameter("bookUserId");
		String buyName=request.getParameter("buyName");
		
		JsonResult result=null;
		try {
			ShopCartServiceImpl impl=new ShopCartServiceImpl();
			ShopCart cart=new ShopCart();
			User user=new User();
			Book book=new Book();
			UUID uuid=UUID.randomUUID();
			cart.setShopcart_id(uuid.toString());
			user.setUserName(userName);
			cart.setUser(user);
			book.setBookPhoto(bookPhoto);
			cart.setBook(book);
			book.setBookdamage(bookdamage);
			cart.setBook(book);
			cart.setShopcart_book_id(shopcart_book_id);
			book.setBookStock(bookStock);
			cart.setBook(book);
			book.setBookremarks(bookremarks);
			cart.setBook(book);
			book.setBookUserId(bookUserId);
			cart.setBook(book);
			cart.setShopcart_order_id("");
			cart.setShopcart_book_name(shopcart_book_name);
			cart.setShopcart_user_id(shopcart_user_id);
			cart.setShopcart_book_price(shopcart_book_price);
			cart.setShopcart_number(shopcart_number);
			cart.setShopcart_subtotal(shopcart_subtotal);
			cart.setShopcart_total(null);
			int i=impl.addToCart(cart);
			if(i>0){
				result=new JsonResult(Constants.STATUS_SUCCESS, "添加成功",cart);
			
			} else  {
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "添加失败");
				
			}
		} catch (Exception e) {
		
			e.printStackTrace();
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}

}
