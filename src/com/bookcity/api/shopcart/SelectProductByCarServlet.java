package com.bookcity.api.shopcart;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.ShopCartServiceImpl;


/**
 * Servlet implementation class SelectProductByCarServlet
 */
@WebServlet("/api/shopCart/SelectProductByCarServlet")
public class SelectProductByCarServlet extends HttpServlet {
	
	/**
	 * request 请求对象
	 * response 响应对象 
	 */
	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String uid=request.getParameter("uid");
		//3)处理业务逻辑
		//String sql="select * from user";
		//创建一个HashMap封装消息 HashMap转换成消息后封装成对象{}  ArrayList转换成json后变成一个数组
		JsonResult result=null;
		try {
			ShopCartServiceImpl impl=new ShopCartServiceImpl();
			List<HashMap<String,Object>> list=impl.selectProductByCar(uid);
			
			if(list.size()>0){
				result=new JsonResult(Constants.STATUS_SUCCESS, "查询成功", list);
				
			}else{
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "您未添加任何图书到购物车",list);
			}
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
		
		
	}

}
