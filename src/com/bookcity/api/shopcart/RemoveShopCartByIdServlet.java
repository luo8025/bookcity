package com.bookcity.api.shopcart;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.ShopCartServiceImpl;



/**
 * Servlet implementation class RemoveShopCartByIdServlet
 */
@WebServlet("/api/shopCart/RemoveShopCartByIdServlet")
public class RemoveShopCartByIdServlet extends HttpServlet {
	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		//request.setCharacterEncoding("utf-8");
		//2)获得数据
		String shopcart_id=request.getParameter("shopcart_id");
		
		//3)处理业务逻辑
		//String sql="delete from user where user_id=? ";
		
		//创建一个HashMap封装消息 HashMap转换成消息后封装成对象{}  ArrayList转换成json后变成一个数组
		JsonResult result=null;
		try {
			ShopCartServiceImpl impl=new ShopCartServiceImpl();
			if(!"".equals(shopcart_id)&&shopcart_id!=null){
				impl.deleteCarShopById(shopcart_id);
				result=new JsonResult(Constants.STATUS_SUCCESS, "删除成功",shopcart_id);
				
			}else{
				result=new JsonResult(Constants.STATUS_NOT_FOUND, "删除失败");
			}
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
		
		
	}

}
