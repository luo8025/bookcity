package com.bookcity.api.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class SelectBookAllServlet
 */
@WebServlet("/api/bookLike/SelectBookAllServlet")
public class LikeSelectBookAllServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	        String name=req.getParameter("name");
	        HttpSession session=req.getSession();
	        session.setAttribute("name", name);
			JsonResult result =null;
			try {
				if(!"".equals(name)&&name!=null){
					
					BookServiceImpl serivceimpl=new BookServiceImpl();
					
					List<HashMap<String,Object>> list=serivceimpl.selectAll("%"+name+"%");//去空
					
					if(list!=null){
						result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",list);
					}else {
						result=new JsonResult(Constants.STATUS_NOT_FOUND,"查询失败","");
					}
				}else{
					result=new JsonResult(Constants.STATUS_NOT_FOUND,"输入不能为空","");
				}
				
			}catch(Exception ex) {
				result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
			}

			JsonResultWriter.writer(resp,result);
		
	}
}
