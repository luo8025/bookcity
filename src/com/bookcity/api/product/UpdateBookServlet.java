package com.bookcity.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Book;
import com.bookcity.model.User;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class UpdateBookServlet
 	* 根据用户id和图书id修改图书信息
 */
@WebServlet("/UpdateBookServlet")
public class UpdateBookServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String bookname=request.getParameter("bookname");
		String bookauthor=request.getParameter("bookauthor");
		String bookprice=request.getParameter("bookprice");
		String bookdamage=request.getParameter("bookdamage");
		String bookcity=request.getParameter("bookcity");
		String bookcategory=request.getParameter("bookcategory");
		String bookPublicationtime=request.getParameter("bookPublicationtime");
		String bookphoto=request.getParameter("bookphoto");
		String bookremarks=request.getParameter("bookremarks");
		String bookstock=request.getParameter("bookstock");
		String bookcountry=request.getParameter("bookcountry");
		String bookid=request.getParameter("bookid");
		String userid=request.getParameter("userid");
		 String bookpdf=request.getParameter("bookpdf");
		
		JsonResult result=null;
		
	
	
		

		
		try {
			Book book=new Book();
			book.setBookName(bookname);
			book.setBookAuthor(bookauthor);
			book.setBookprice(bookprice);
			book.setBookdamage(bookdamage);
			book.setBookCity(bookcity);
			book.setBookCategory(bookcategory);
			book.setBookPublicationTime(bookPublicationtime);
			book.setBookPhoto(bookphoto);
			book.setBookremarks(bookremarks);
		    book.setBookStock(bookstock);
			book.setBookCountry(bookcountry);
			book.setBookId(bookid);
			book.setBookPdf(bookpdf);
			User user=new User();
			user.setUserId(userid);
			book.setUser(user);
			
				
				if(!"".equals(book.getUser().getUserId())&&userid!=null){
					BookServiceImpl serivceimpl=new BookServiceImpl();
					serivceimpl.UpdateBook(book);
					result=new JsonResult(Constants.STATUS_SUCCESS, "修改成功",book);
					
				}else{
					result=new JsonResult(Constants.STATUS_NOT_FOUND, "修改失败");
				}
			} catch (Exception e) {
				result=new JsonResult(Constants.STATUS_FAIL, "网络错误",e.getMessage());
			}
		
         
		JsonResultWriter.writer(resp,result);
		
		}
	
	}	
	

