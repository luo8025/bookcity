package com.bookcity.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Book;
import com.bookcity.model.User;
import com.bookcity.service.BookServiceImpl;





/**
 * Servlet implementation class AddBookServlet
 * 上架图书
 */
@WebServlet("/AddBookServlet")
public class AddBookServlet extends HttpServlet {
	
	@Override
		protected void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String bookname=request.getParameter("bookname");
		String bookauthor=request.getParameter("bookauthor");
		String bookprice=request.getParameter("bookprice");
		String bookdamage=request.getParameter("bookdamage");
		String bookcity=request.getParameter("bookcity");
		String bookcategory=request.getParameter("bookcategory");
		String bookPublicationtime=request.getParameter("bookPublicationtime");
		String bookphoto=request.getParameter("bookphoto");
		String bookremarks=request.getParameter("bookremarks");
	    String bookstock=request.getParameter("bookstock");
		String bookcountry=request.getParameter("bookcountry");
	    String userid=request.getParameter("userid");
	    String bookpdf=request.getParameter("bookpdf");
		JsonResult result=null;
		
		try {
			BookServiceImpl serivceimpl=new BookServiceImpl();
			Book book=new Book();
			User user=new User();
			book.setBookName(bookname);
			book.setBookAuthor(bookauthor);
			book.setBookprice(bookprice);
			book.setBookdamage(bookdamage);
			book.setBookCity(bookcity);
			book.setBookCategory(bookcategory);
			book.setBookPublicationTime(bookPublicationtime);
			book.setBookPhoto(bookphoto);
			book.setBookremarks(bookremarks);
		    book.setBookStock(bookstock);
			book.setBookCountry(bookcountry);
			
			user.setUserId(userid);
			  book.setUser(user);
            book.setBookPdf(bookpdf);
			serivceimpl.insert(book);
			try {
			
				result=new JsonResult(Constants.STATUS_SUCCESS,"上架成功");
			}catch(Exception ex) {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"上架失败","");
			}
				
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"上架异常",ex.getMessage());
		}
		
         
		JsonResultWriter.writer(resp,result);
		
		}
	
	

	
    
	}


