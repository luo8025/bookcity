package com.bookcity.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.model.Book;
import com.bookcity.model.User;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class UpBookServlet
 */
@WebServlet("/UpBookServlet")
public class UpBookServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String id=request.getParameter("bookid");
		com.bookcity.common.JsonResult result=null;
		try {
			BookServiceImpl SerivceImpl=new BookServiceImpl();
			SerivceImpl.shangbook(id);
			try {
				result=new JsonResult(Constants.STATUS_SUCCESS,"上架成功");
			}catch(Exception ex) {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"上架失败","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"上架异常",ex.getMessage());
		}
		JsonResultWriter.writer(resp,result);
		
	}

}
