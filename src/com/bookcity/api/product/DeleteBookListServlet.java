package com.bookcity.api.product;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.AdminServiceImpl;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class AdminDeleteUserservlet
 * ��idɾ���û�
 */
@WebServlet("/DeleteBookListServlet")
public class DeleteBookListServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String bookid=req.getParameter("bookid");
		JsonResult result=null;
		try {
			BookServiceImpl bookserviceimpl =new BookServiceImpl();
			bookserviceimpl.deletebook(bookid);
			if(bookid!=null){
				result=new JsonResult(Constants.STATUS_SUCCESS,"ɾ���ɹ�");
				}
			else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"ɾ��ʧ��","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"ɾ���쳣",ex.getMessage());
		}

		JsonResultWriter.writer(resp,result);
	

	}
}