package com.bookcity.api.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.common.PageModel;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class SelectBookCountryServlet
 * 按国家查询
 */
@WebServlet("/SelectBookCountryServlet")
public class SelectBookCountryServlet extends HttpServlet {
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String country=req.getParameter("country");
        String page=req.getParameter("page");
		String pageSize=req.getParameter("pageSize");
		JsonResult result =null;
		try {
			
			BookServiceImpl serivceimpl=new BookServiceImpl();
			PageModel<List> model=serivceimpl.selectcountry(page, pageSize, country);
		
			
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",model);
				
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}

		JsonResultWriter.writer(resp,result);
	
    }

}
