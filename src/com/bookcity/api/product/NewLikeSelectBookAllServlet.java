package com.bookcity.api.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.common.PageModel;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class NewLikeSelectBookAllServlet
 */
@WebServlet("/api/newBook/NewLikeSelectBookAllServlet")
public class NewLikeSelectBookAllServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			String name=req.getParameter("name");
			String page=req.getParameter("page");
			String pageSize=req.getParameter("pageSize");
			JsonResult result =null;
			try {
				
				String regEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(name);
				String names= m.replaceAll("").trim();
				
				
				
				BookServiceImpl serivceimpl=new BookServiceImpl();
				PageModel<List> model=serivceimpl.selectNewAll("%"+names+"%", page, pageSize);
					result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",model);
			}catch(Exception ex) {
				result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
			}

			JsonResultWriter.writer(resp,result);
		
	}
}
