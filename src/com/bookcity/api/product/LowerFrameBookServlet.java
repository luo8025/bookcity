package com.bookcity.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.BookServiceImpl;



/**
 * Servlet implementation class LowerFrameBookServlet
 * 按id下架商品
 */
@WebServlet("/LowerFrameBookServlet")
public class LowerFrameBookServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id=req.getParameter("bookid");
		com.bookcity.common.JsonResult result=null;
		try {
			BookServiceImpl SerivceImpl=new BookServiceImpl();
			SerivceImpl.LowerFramebook(id);
			try {
				result=new JsonResult(Constants.STATUS_SUCCESS,"下架成功");
			}catch(Exception ex) {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"下架失败","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"下架异常",ex.getMessage());
		}
		JsonResultWriter.writer(resp,result);
		
	}
}
