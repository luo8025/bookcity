package com.bookcity.api.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class SelectIdBookServlet
 */
@WebServlet("/SelectIdBookServlet")
public class SelectIdBookServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String bookid=req.getParameter("bookid");
		com.bookcity.common.JsonResult result=null;
		try {
			BookServiceImpl SerivceImpl=new BookServiceImpl();
			List<HashMap<String,Object>> list=  SerivceImpl.selectidbook(bookid);
			if(list!=null) {
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",list);
			}else {
				result=new JsonResult(Constants.STATUS_NOT_FOUND,"查询失败","");
			}
		}catch(Exception ex) {
			result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}
		JsonResultWriter.writer(resp,result);
	}

}
