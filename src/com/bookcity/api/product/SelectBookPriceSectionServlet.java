package com.bookcity.api.product;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.common.PageModel;
import com.bookcity.dao.BookDaoImpl;
import com.bookcity.service.OrderServiceImpl;
import com.google.gson.Gson;



/**
 * Servlet implementation class newdemo
 */
@WebServlet("/api/book/SelectBookPriceSectionServlet")
public class SelectBookPriceSectionServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonResult result=null;
		String start=request.getParameter("start");
		String end=request.getParameter("end");
		String page=request.getParameter("page");
		String pageSize=request.getParameter("pageSize");
		
		try {
			BookDaoImpl bookdaoimpl=new BookDaoImpl();
			PageModel<List> model=bookdaoimpl.selectpricesection(page, pageSize, start, end);
			
				result=new JsonResult(Constants.STATUS_SUCCESS, "查询成功",model );
		
		}catch(Exception ex){
			result=new JsonResult(Constants.STATUS_FAIL,"查询异常",ex.getMessage());
		}
		
		JsonResultWriter.writer(response, result);
	}
}