package com.bookcity.api.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookcity.common.Constants;
import com.bookcity.common.JsonResult;
import com.bookcity.common.JsonResultWriter;
import com.bookcity.common.PageModel;
import com.bookcity.service.BookServiceImpl;

/**
 * Servlet implementation class SelectBookCityServlet
 */
@WebServlet("/SelectBookCityServlet")
public class SelectBookCityServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
	     String city=request.getParameter("city");
	     String page=request.getParameter("page");
		 String pageSize=request.getParameter("pageSize");
				com.bookcity.common.JsonResult result =null;
				try {
					
					BookServiceImpl serivceimpl=new BookServiceImpl();
					PageModel<List> model=serivceimpl.selectbookcity(page, pageSize, city);
				
						result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",model);
						
				}catch(Exception ex) {
					result=new JsonResult( Constants.STATUS_FAIL,"查询异常",ex.getMessage());
				}

				JsonResultWriter.writer(resp,result);
			
		}
		
	
}
