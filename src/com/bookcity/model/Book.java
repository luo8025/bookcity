package com.bookcity.model;

public class Book {
	private String BookName;
	 private String BookAuthor;
	 private String Bookprice;
	
	private String Bookdamage;
	 private String BookCity;
	 private String BookCategory;
	 private String BookPublicationTime;
	 private String BookPhoto;
	 private String Bookremarks;
	 private String BookIssale;
	
	private String BookStock;
	 private String BookSaletime;
	 private String BookDismounttime;
	 private String BookCountry;
	 private String BookUserId;
	 private String BookPdf;
	 private String BookSalequantity;
	 public String getBookSalequantity() {
		return BookSalequantity;
	}
	public void setBookSalequantity(String bookSalequantity) {
		BookSalequantity = bookSalequantity;
	}

	
	 public String getBookPdf() {
		return BookPdf;
	}
	public void setBookPdf(String bookPdf) {
		BookPdf = bookPdf;
	}
	public String getBookUserId() {
		return BookUserId;
	}
	public void setBookUserId(String bookUserId) {
		BookUserId = bookUserId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public User user;
	
	 public String getBookCountry() {
		return BookCountry;
	}
	public void setBookCountry(String bookCountry) {
		BookCountry = bookCountry;
	}
	public String getBookdamage() {
			return Bookdamage;
		}
		public void setBookdamage(String bookdamage) {
			Bookdamage = bookdamage;
		}
	
	private String BookId;
	 public String getBookId() {
		return BookId;
	}
	public void setBookId(String bookId) {
		BookId = bookId;
	}
	public String getBookName() {
		return BookName;
	}
	public void setBookName(String bookName) {
		BookName = bookName;
	}
	public String getBookAuthor() {
		return BookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		BookAuthor = bookAuthor;
	}
	public String getBookprice() {
		return Bookprice;
	}
	public void setBookprice(String bookprice) {
		Bookprice = bookprice;
	}
	
	public String getBookCity() {
		return BookCity;
	}
	public void setBookCity(String bookCity) {
		BookCity = bookCity;
	}
	public String getBookCategory() {
		return BookCategory;
	}
	public void setBookCategory(String bookCategory) {
		BookCategory = bookCategory;
	}
	public String getBookPublicationTime() {
		return BookPublicationTime;
	}
	public void setBookPublicationTime(String bookPublicationTime) {
		BookPublicationTime = bookPublicationTime;
	}
	public String getBookPhoto() {
		return BookPhoto;
	}
	public void setBookPhoto(String bookPhoto) {
		BookPhoto = bookPhoto;
	}
	public String getBookremarks() {
		return Bookremarks;
	}
	public void setBookremarks(String bookremarks) {
		Bookremarks = bookremarks;
	}

	public String getBookIssale() {
		return BookIssale;
	}
	public void setBookIssale(String bookIssale) {
		BookIssale = bookIssale;
	}

	 public String getBookStock() {
			return BookStock;
		}
		public void setBookStock(String bookStock) {
			BookStock = bookStock;
		}

	public String getBookSaletime() {
		return BookSaletime;
	}
	public void setBookSaletime(String bookSaletime) {
		BookSaletime = bookSaletime;
	}
	public String getBookDismounttime() {
		return BookDismounttime;
	}
	public void setBookDismounttime(String bookDismounttime) {
		BookDismounttime = bookDismounttime;
	}
	
}
