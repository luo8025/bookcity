package com.bookcity.model;

public class User {
	private String userId;
	private String userName;
	private String userPassword;
	private String userAddress;
	private String userTelphone;
	private String userEmail;
	private String userSex;
	private String userAge;
	private String userPhoto;
	private String userRegisterTime;
	private String userstatus;
	private String userbantime;
	private String userbanreason;
	private String userpraise;
	private String userbad;
	
	public String getUserbad() {
		return userbad;
	}
	public void setUserbad(String userbad) {
		this.userbad = userbad;
	}
	
	public String getUserpraise() {
		return userpraise;
	}
	public void setUserpraise(String userpraise) {
		this.userpraise = userpraise;
	}
	public String getUserstatus() {
		return userstatus;
	}
	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}
	public String getUserbantime() {
		return userbantime;
	}
	public void setUserbantime(String userbantime) {
		this.userbantime = userbantime;
	}
	public String getUserbanreason() {
		return userbanreason;
	}
	public void setUserbanreason(String userbanreason) {
		this.userbanreason = userbanreason;
	}
	public String getUserRegisterTime() {
		return userRegisterTime;
	}
	public void setUserRegisterTime(String userRegisterTime) {
		this.userRegisterTime = userRegisterTime;
	}
	public String getUserPhoto() {
		return userPhoto;
	}
	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	public String getUserTelphone() {
		return userTelphone;
	}
	public void setUserTelphone(String userTelphone) {
		this.userTelphone = userTelphone;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserSex() {
		return userSex;
	}
	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}
	public String getUserAge() {
		return userAge;
	}
	public void setUserAge(String userAge) {
		this.userAge = userAge;
	}
	
	
}
