package com.bookcity.model;

public class Address {
	private String addressName;
	private String addressId;
	private String addressUserId;
	
	private String addressUserTel;
	
	
	
	public String getAddressUserTel() {
		return addressUserTel;
	}
	public void setAddressUserTel(String addressUserTel) {
		this.addressUserTel = addressUserTel;
	}
	
	
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getAddressUserId() {
		return addressUserId;
	}
	public void setAddressUserId(String addressUserId) {
		this.addressUserId = addressUserId;
	}
	

}
