package com.bookcity.model;

public class Order {
	private String orderid;
	private String orderUserId;
	private String orderUserName;
	private String orderTime;
	private String orderIsValid;
	private String orderTotal;
	private String orderSendPlace;
	private String orderBookId;
	private ShopCart shopCart;
	
	public ShopCart getShopCart() {
		return shopCart;
	}
	public void setShopCart(ShopCart shopCart) {
		this.shopCart = shopCart;
	}
	
	public String getOrderBookId() {
		return orderBookId;
	}
	public void setOrderBookId(String orderBookId) {
		this.orderBookId = orderBookId;
	}
	private Book book;
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public String getOrderSendPlace() {
		return orderSendPlace;
	}
	public void setOrderSendPlace(String orderSendPlace) {
		this.orderSendPlace = orderSendPlace;
	}
	
	
	
	
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getOrderUserId() {
		return orderUserId;
	}
	public void setOrderUserId(String orderUserId) {
		this.orderUserId = orderUserId;
	}
	
	public String getOrderUserName() {
		return orderUserName;
	}
	public void setOrderUserName(String orderUserName) {
		this.orderUserName = orderUserName;
	}
	
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getOrderIsValid() {
		return orderIsValid;
	}
	public void setOrderIsValid(String orderIsValid) {
		this.orderIsValid = orderIsValid;
	}
	public String getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(String orderTotal) {
		this.orderTotal = orderTotal;
	}
	

}
