package com.bookcity.model;

public class ShopCart {
	private String shopcart_book_id;
	private String shopcart_book_name;
	private String shopcart_user_id;
	private String shopcart_book_price;
	private String shopcart_number;
	private String shopcart_subtotal;
	private String shopcart_total;
	private String shopcart_id;
	private String shopcart_order_id;
	private String shopcart_creattime;
	private Book book;
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public String getShopcart_book_id() {
		return shopcart_book_id;
	}
	public void setShopcart_book_id(String shopcart_book_id) {
		this.shopcart_book_id = shopcart_book_id;
	}
	public String getShopcart_book_name() {
		return shopcart_book_name;
	}
	public void setShopcart_book_name(String shopcart_book_name) {
		this.shopcart_book_name = shopcart_book_name;
	}
	public String getShopcart_user_id() {
		return shopcart_user_id;
	}
	public void setShopcart_user_id(String shopcart_user_id) {
		this.shopcart_user_id = shopcart_user_id;
	}
	public String getShopcart_book_price() {
		return shopcart_book_price;
	}
	public void setShopcart_book_price(String shopcart_book_price) {
		this.shopcart_book_price = shopcart_book_price;
	}
	public String getShopcart_number() {
		return shopcart_number;
	}
	public void setShopcart_number(String shopcart_number) {
		this.shopcart_number = shopcart_number;
	}
	public String getShopcart_subtotal() {
		return shopcart_subtotal;
	}
	public void setShopcart_subtotal(String shopcart_subtotal) {
		this.shopcart_subtotal = shopcart_subtotal;
	}
	public String getShopcart_total() {
		return shopcart_total;
	}
	public void setShopcart_total(String shopcart_total) {
		this.shopcart_total = shopcart_total;
	}
	public String getShopcart_id() {
		return shopcart_id;
	}
	public void setShopcart_id(String shopcart_id) {
		this.shopcart_id = shopcart_id;
	}
	public String getShopcart_order_id() {
		return shopcart_order_id;
	}
	public void setShopcart_order_id(String shopcart_order_id) {
		this.shopcart_order_id = shopcart_order_id;
	}
	public String getShopcart_creattime() {
		return shopcart_creattime;
	}
	public void setShopcart_creattime(String shopcart_creattime) {
		this.shopcart_creattime = shopcart_creattime;
	}
	
	
}
