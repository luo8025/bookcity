package com.bookcity.service;

import java.util.HashMap;
import java.util.List;

import com.bookcity.dao.ShopCartDaoImpl;
import com.bookcity.model.ShopCart;

public class ShopCartServiceImpl {
	
	
	
	ShopCartDaoImpl impl=new ShopCartDaoImpl();
	/*
	 * 加入购物车
	 */
	public int addToCart(ShopCart sc){
		return impl.addToCart(sc);
	}
	/*
	 * 查看购物车
	 */
	public List<HashMap<String,Object>> selectProductByCar(String id){
		return impl.selectProductByCar(id);
	}
	/**
	 * 移除购物车
	 */
	public void deleteCarShopById(String id){
		impl.deleteCarShopById(id);
	}
}
