package com.bookcity.service;

import java.util.HashMap;
import java.util.List;

import com.bookcity.dao.AddressDaoImpl;
import com.bookcity.model.Address;
import com.bookcity.model.Book;

public class AddressServiceImpl {
	AddressDaoImpl adressdaoimpl=new AddressDaoImpl();
	//查询用户地址
	public List<HashMap<String, Object>> selectaddressall(String id) {
		List<HashMap<String,Object>>list=adressdaoimpl.selectaddressall(id);
		return list;
	}
	//添加地址
	public int insert(Address address) {
    	int i=adressdaoimpl.insert(address);
    	return i;
    }
	//删除地址
		public int delete(String userId) {
	    	int i=adressdaoimpl.delete(userId);
	    	return i;
	    }

}
