package com.bookcity.service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.bookcity.dao.OrderDaoImpl;
import com.bookcity.model.Order;
import com.bookcity.unitl.SqlHelp;

public class OrderServiceImpl {
	OrderDaoImpl orderdaoimpl=new OrderDaoImpl();
	
	/*
	 * 结算
	 */
	public void settlement(Order o){
		UUID uuid=UUID.randomUUID();
		o.setOrderid(uuid.toString());
		orderdaoimpl.settlement(o);
		orderdaoimpl.updateCar(uuid.toString(), o.getOrderUserId(),o.getShopCart().getShopcart_id());
	}
	
	
	//查询购买订单
	public List<HashMap<String, Object>> selectbuy(String orderUserId) {
		return orderdaoimpl.selectbuy(orderUserId);
	}
	//查询出售订单
	public List<HashMap<String, Object>> selectsell(String orderUserId) {
		return orderdaoimpl.selectsell(orderUserId);
	}
	//取消订单
	public void cancelorder(String orderId,String OrderuserId) {
		 orderdaoimpl.cancelorder(orderId, OrderuserId);
	}
	//删除订单
	public void deleteorder(String orderId) {
		 orderdaoimpl.deleteorder(orderId);
	}
	//查询所有订单
	public List<HashMap<String, Object>> selectallorder() {
		return orderdaoimpl.selectAllorder();
	}
	 /*
	  * 插入地址
	  */
	 public int insertAddress(String address,String id){
		return  orderdaoimpl.insertAddress(address, id);
	 }
	
	

}
