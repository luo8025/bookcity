package com.bookcity.service;

import java.util.HashMap;
import java.util.List;

import com.bookcity.common.PageModel;
import com.bookcity.dao.AdminDaoImpl;
import com.bookcity.dao.LoginDaoImpl;
import com.bookcity.model.Admin;
import com.bookcity.model.User;

public class AdminServiceImpl {
	   AdminDaoImpl DaoImpl=new AdminDaoImpl();
         //查询全部用户信息
	    public  PageModel<List> selectuser(String page,String pageSize){
	    	PageModel model=DaoImpl.selectuser(page,pageSize);
	    	return model;
	    }
	    //按用户id封停账号
	    public void titleuser(String userid) {
	    	DaoImpl.Titleuser(userid);
	    }
	    //按用户id解封账号
	    public void unlockuser(String userid) {
	    	DaoImpl.Unlockuser(userid);
	    }
	    //按用户id删除账号
	    public void deleteuser(String userid) {
	    	DaoImpl.Deleteuser(userid);
	    }
	    //按id查询一个用户的信息
	    public User selectuserid(String userid) {
	    	return DaoImpl.selectuserid(userid);
	    } 
	    //按天统计售卖金额
	    public List<HashMap<String, Object>> selectSales() {
	    	return DaoImpl.selectSales();
	    	  
	    } 
	  //按月统计售卖金额
	    public List<HashMap<String, Object>> selectSalesmonth() {
	    	return DaoImpl.selectSalesmonth();
	    	  
	    } 
	  //按年统计售卖金额
	    public List<HashMap<String, Object>> selectSalesyear() {
	    	return DaoImpl.selectSalesyear();
	    	  
	    } 
	  //按种类统计数量
	    public List<HashMap<String, Object>> selectcategorycount() {
	    	return DaoImpl.selectcategorycount();
	    }
	    //按城市售卖统计数量
	    public List<HashMap<String, Object>> selectbookcity() {
	    	return DaoImpl.selectbookcity();
	    }
	  //统计用户所在城市
	    public List<HashMap<String, Object>> selectusercity() {
	    	return DaoImpl.selectusercity();
	    }
		public Admin loginAdmin(String adminname ,String password){
			return DaoImpl.loginAdmin(adminname,  password);
		}
		//查询所有管理员
		public List<HashMap<String, Object>> selectalladmin() {
			return DaoImpl.selectalladmin();
		}
		//统计加入购物车种类
	    public List<HashMap<String, Object>> selectshopcatcount() {
	    	return DaoImpl.selectshopactcount();
	    	  
	    } 
	  //统计加入订单书
	    public List<HashMap<String, Object>> selectordercount() {
	    	return DaoImpl.selectordercount();
	    	  
	    }
}
