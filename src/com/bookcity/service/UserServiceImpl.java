package com.bookcity.service;


import java.util.HashMap;
import java.util.List;

import com.bookcity.dao.UserDaoImpl;
import com.bookcity.model.User;

public class UserServiceImpl {
	
	UserDaoImpl impl=new UserDaoImpl();
	
	/*
	 * 修改用户密码
	 */
	public void modifyUserPassword(String password,String id){
		impl.modifyUserPassword(password,id);
	}

	/**
	 * 用户注册
	 */
	public int userRegister(User user){
		 int i =impl.userRegister(user);
		 return i;
	}
	/*
	 * 查看个人信息
	 */
	public User selectUserInformation(String userId){
		return impl.selectUserInformation(userId);
	}
	/**
	 * 修改头像
	 * @param userName
	 * @return
	 */
	public void headportraituser(String photo,String userid) {
		impl.headportraituser(photo, userid);
	}
	
	
	/*
	 * 忘记密码
	 */
	public  List<HashMap<String,Object>> forgetPwd(String userName){
		return impl.forgetPwd(userName);
	}
	/*
	 * 忘记密码2
	 */
	public List<HashMap<String,Object>> forgetPwd2(String userName,String telphone){
		return impl.forgetPwd2(userName, telphone);
	}
	/*
	 * 忘记密码3
	 */
	public int forgetPwd3(String password,String userId){
		int i=impl.forgetPwd3(password,userId);
		return i;
	}
}
