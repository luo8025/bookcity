package com.bookcity.service;

import java.util.HashMap;
import java.util.List;

import com.bookcity.common.PageModel;
import com.bookcity.dao.BookDaoImpl;
import com.bookcity.model.Book;



public class BookServiceImpl {
	    BookDaoImpl daoImpl=new BookDaoImpl();
	    
	    //鏂板鍥句功
	       public void insert(Book book) {
	    	
	    	   daoImpl.insert(book); 
	       }
	       
	    //涓嬫灦鍥句功
	       public void LowerFramebook(String id) {
	    	   daoImpl.LowerFramebook(id);
	       }
	       //上架
	       public void shangbook(String bookid) {
	    	   daoImpl.shangbook(bookid);
	       }
	       
	       
	    //淇敼鍥句功淇℃伅
	       public void UpdateBook(Book book) {
	    	   daoImpl.updatebook(book);
	       }
	    //鎸塽serid鏌ヨ鐢ㄦ埛鑷繁涓婃灦鍥句功
	       public List<HashMap<String,Object>> selectbook(String userid){
	    	   return daoImpl.selectuserid(userid);
	       }
	     //纾ㄦ崯绋嬪害
//	       public List<HashMap<String, Object>> selectdamage() {
//	   		return daoImpl.selectdamage();
	   		public PageModel<List> selectdamage(String page,String pageSize) {
		    	  PageModel model= daoImpl.selectdamage(page, pageSize);
			   		return model;
			   	}
	    //浠锋牸闄嶅簭
	       public PageModel<List> selectpricedown(String page,String pageSize) {
	    	  PageModel model= daoImpl.selectpricedown(page, pageSize);
		   		return model;
		   	}
	       //浠锋牸鍗囧簭
	       public PageModel<List> selectpriceup(String page,String pageSize) {
		    	  PageModel model= daoImpl.selectpriceup(page, pageSize);
			   		return model;
			   	}
	     //浠锋牸鍖洪棿
	       public PageModel<List> selectpricesection(String page,String pageSize,String start,String end) {
	    	   PageModel model=   daoImpl.selectpricesection(page, pageSize, start, end);
	    	   return model;
		   	}
	       //鍦板尯

	       public PageModel<List> selectbookcity(String page,String pageSize,String city) {
	    	   PageModel model= daoImpl.selectusercity(page, pageSize, city);
	    	   return model;
		   	}
	       //绉嶇被

	       public PageModel<List> selectbookcategory(String page,String pageSize,String category) {
	    	   PageModel model= daoImpl.selectcategory(page, pageSize, category);
	    	   return model;
		   	}
	       //鍥藉
	       public PageModel<List> selectbookcountry(String page,String pageSize,String country) {
	    	   PageModel model= daoImpl.selectcountry(page, pageSize, country);
	    	   return model;
		   	}
	       /*
	        * 鎬绘煡璇�  妯＄硦鏌ヨ
	        */
	       public  List<HashMap<String,Object>> selectAll(String name){
	    	   return daoImpl.selectAll(name);
	       }
	       
	       /*
	 	   * 妯＄硦鏌ヨ缁撴灉浣滀负鏂拌〃
	 	   */
	       public PageModel<List>  selectNewAll(String name,String page,String pageSize){
	    	  PageModel model=daoImpl.selectNewAll(name, page, pageSize);
	    	  return model;
	       }
//	       public List<HashMap<String,Object>> selectbookcountry(String country){
//	    	   return daoImpl.selectcountry(country);
//	       }
	       public PageModel<List> selectcountry(String page,String pageSize,String country) {
	    	   PageModel model= daoImpl.selectcountry(page, pageSize, country);
	    	   return model;
		   	}
	       //图书id搜索
	 	  public List<HashMap<String,Object>> selectidbook(String bookid){
	    	   return daoImpl.selectbook(bookid);
	       }
	 	 //按种类统计数量
		    public List<HashMap<String, Object>> selectbookcategory() {
		    	return daoImpl.selectbookcategory();
		    	  
		    }
		  //搜索图书
		    public List<HashMap<String, Object>> selectbookallnopage() {
		    	return daoImpl.selectbookallnopage();
		    	  
		    }
		    //按上架时间
		    public PageModel<List> selecttime(String page,String pageSize){
		    	return daoImpl.selecttime(page, pageSize);
		    }
		    //刪除商家上架商品
		    public void deletebook(String bookid) {
		    	daoImpl.Deletebook(bookid);
		    }


}
