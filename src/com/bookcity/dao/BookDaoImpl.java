package com.bookcity.dao;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bookcity.common.PageModel;
import com.bookcity.model.Book;
import com.bookcity.unitl.SqlHelp;



public class BookDaoImpl {
	//新增图书
	  public void insert(Book book) {
			String sql="insert into book VALUES(UUID(),?,?,?,?,?,?,?,?,?,1,?,NOW(),?,?,?,?,0)" ;
			SqlHelp.update(sql,book.getBookName(),book.getBookAuthor(),book.getBookprice(),book.getBookdamage(),book.getBookCity(),book.getBookCategory(),book.getBookPublicationTime(),book.getBookPhoto(),book.getBookremarks(),book.getBookStock(),null,book.getBookCountry(),book.getUser().getUserId(),book.getBookPdf());	
			
			 }
	  //按图书id下架商品
	  public void LowerFramebook(String id){
			String sql="update book set book_is_sale=0 ,book_dismounttime=NOW() where book_id=?";
			SqlHelp.update(sql, id);
		}
	  //上架商品
	  public void shangbook(String bookid) {
		  String sql="update book set book_is_sale=1 ,book_dismounttime=null where book_id=?";
		  SqlHelp.update(sql, bookid);
	  }
	  
	  //按图书id修改图书
	  public void updatebook(Book book) {
		  String sql="update book set book_name=? ,book_author=?,book_price=?,book_damage=?,book_city=?,book_category=?,book_photo=?,book_remarks=?,book_stock=?,book_country=?,book_pdf=? where book_id=? and book_user_id=?";
		  SqlHelp.update(sql,book.getBookName(),book.getBookAuthor(),book.getBookprice(),book.getBookdamage(),book.getBookCity(),book.getBookCategory(),book.getBookPhoto(),book.getBookremarks(),book.getBookStock(),book.getBookCountry(),book.getBookPdf(),book.getBookId(),book.getUser().getUserId());
	  }
	  //按用户id查询全部上架图书
	  public List<HashMap<String,Object>> selectuserid(String userid) {
			

			String sql="select * from book where book_user_id=? ORDER BY book_saletime DESC";

	    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql,userid);
	    		return list;
	  } 
	  //折旧程度

	  public PageModel<List> selectdamage(String page,String pageSize) {
			String sql="SELECT *FROM book where book_is_sale=1 ORDER BY book_damage DESC";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			List<HashMap<String ,Object>> list=SqlHelp.select3(pagemodel.toMysqlSql());
			
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //价格降序
	  public PageModel<List> selectpricedown(String page,String pageSize) {
			String sql="SELECT *FROM book where  book_is_sale=1 ORDER BY book_price DESC";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			List<HashMap<String ,Object>> list=SqlHelp.select3(pagemodel.toMysqlSql());
			
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //价格升序
	  public PageModel<List> selectpriceup(String page,String pageSize) {
			String sql="SELECT *FROM book where  book_is_sale=1 ORDER BY book_price ASC";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			List<HashMap<String ,Object>> list=SqlHelp.select3(pagemodel.toMysqlSql());
			
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //价格区间
	  public PageModel<List> selectpricesection(String page,String pageSize,String start,String end) {
			String sql="SELECT* FROM book WHERE  book_is_sale=1  and book_price >"+start+" and book_price<"+end;
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			String sqlNew=pagemodel.toMysqlSql();
			System.out.println(sqlNew);
			List<HashMap<String ,Object>> list=SqlHelp.select3(sqlNew);
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //地区搜索
	  public PageModel<List> selectusercity(String page,String pageSize,String city) {
			String sql="select * from book where book_city="+"'"+city+"'";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			String sqlNew=pagemodel.toMysqlSql();
			System.out.println(sqlNew);
			List<HashMap<String ,Object>> list=SqlHelp.select3(sqlNew);
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //种类搜索
	  public PageModel<List> selectcategory(String page,String pageSize,String category) {
			String sql="select * from book where book_category="+"'"+category+"'";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			String sqlNew=pagemodel.toMysqlSql();
			System.out.println(sqlNew);
			List<HashMap<String ,Object>> list=SqlHelp.select3(sqlNew);
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //按图书id查询图书
	  public List<HashMap<String,Object>> selectbook(String id) {
		  String sql="SELECT b.*,u.user_name from book b LEFT JOIN `user` u on b.book_user_id=u.user_id  where b.book_id=?  and book_is_sale=1";
		  List<HashMap<String,Object>> list= SqlHelp.select3(sql,id);
		  return list;
	  }
	  /**
	   * 总查询  模糊查询
	   * @param name
	   * @return
	   */
	  public  List<HashMap<String,Object>> selectAll(String name){
		  String sql="select * from book where (book_name like ? or book_author like ?) and book_is_sale=1";
		  List<HashMap<String,Object>> list=SqlHelp.select3(sql,name,name );
		  return list;
	  }
	  /*
	   * 模糊查询结果作为新表
	   */
	  public  PageModel<List> selectNewAll(String name,String page,String pageSize){
		  String sql="select * from (select * from book where book_name like "+"'"+ name+"'"+" or book_author like "+"'"+name+"'"+" ) s  where  book_is_sale=1";
		  PageModel pagemodel=new PageModel(sql,page,pageSize);
		  String sqlNew=pagemodel.toMysqlSql();
		  List<HashMap<String ,Object>> list=SqlHelp.select3(sqlNew);
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		  
	  }
	  //国家搜索
	  public PageModel<List> selectcountry(String page,String pageSize,String country) {
			String sql="select * from book where book_country="+"'"+country+"'";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			String sqlNew=pagemodel.toMysqlSql();
			System.out.println(sqlNew);
			List<HashMap<String ,Object>> list=SqlHelp.select3(sqlNew);
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
		}
	  //按种类统计数量
	  public List<HashMap<String,Object>> selectbookcategory(){
			String sql="SELECT book_category category ,count(book_category ) count FROM book GROUP BY book_category";
	    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
	    		return list;
		 
	 }
	  //图书信息
	  public List<HashMap<String,Object>> selectbookallnopage(){
			String sql="SELECT * FROM book WHERE book_is_sale=1  ORDER BY book_saletime";
	    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
	    		return list;
		 
	 }
	  //上架商品
	  public PageModel<List> selecttime(String page,String pageSize) {
			String sql="SELECT *FROM book where book_is_sale=1 ORDER BY book_saletime DESC";
			PageModel pagemodel=new PageModel(sql,page,pageSize);
			List<HashMap<String ,Object>> list=SqlHelp.select3(pagemodel.toMysqlSql());
			List<Book> list2=new ArrayList<Book>();
			if(list.size()>0) {
				for(HashMap<String ,Object> map:list) {
					Book book =new Book();
					book.setBookId(map.get("book_id")==null?null:map.get("book_id").toString());
					book.setBookName(map.get("book_name")==null?null:map.get("book_name").toString());
					book.setBookAuthor(map.get("book_author")==null?null:map.get("book_author").toString());
					book.setBookdamage(map.get("book_damage")==null?null:map.get("book_damage").toString());
					book.setBookprice(map.get("book_price")==null?null:map.get("book_price").toString());
					book.setBookCity(map.get("book_city")==null?null:map.get("book_city").toString());
					book.setBookCategory(map.get("book_category")==null?null:map.get("book_category").toString());
					book.setBookPublicationTime(map.get("book_publicationtime")==null?null:map.get("book_publicationtime").toString());
					book.setBookPhoto(map.get("book_photo")==null?null:map.get("book_photo").toString());
					book.setBookremarks(map.get("book_remarks")==null?null:map.get("book_remarks").toString());
					book.setBookIssale(map.get("book_is_sale")==null?null:map.get("book_is_sale").toString());
					book.setBookStock(map.get("book_stock")==null?null:map.get("book_stock").toString());
					book.setBookSaletime(map.get("book_saletime")==null?null:map.get("book_saletime").toString());
					book.setBookDismounttime(map.get("book_dismounttime")==null?null:map.get("book_dismounttime").toString());
					book.setBookCountry(map.get("book_country")==null?null:map.get("book_country").toString());
					book.setBookUserId(map.get("book_user_id")==null?null:map.get("book_user_id").toString());
					book.setBookPdf(map.get("book_pdf")==null?null:map.get("book_pdf").toString());
					book.setBookSalequantity(map.get("book_salequantity")==null?null:map.get("book_salequantity").toString());
					list2.add(book);
				}
			}
			pagemodel.setList(list2);
			List<HashMap<String ,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			
			String total=countlist.get(0).get("count").toString();
			pagemodel.setTotal(Integer.valueOf(total));
			return pagemodel;
	  }
	  //删除上架商品
	  public void Deletebook(String bookid) {
			 String sql="delete from book where book_id=?";
			 SqlHelp.update(sql, bookid);
		 }
	

}
