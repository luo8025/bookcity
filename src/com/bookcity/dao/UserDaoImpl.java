package com.bookcity.dao;


import java.util.HashMap;
import java.util.List;

import com.bookcity.model.User;
import com.bookcity.unitl.SqlHelp;

public class UserDaoImpl {
	/*
	 * 修改用户密码
	 */
	public void modifyUserPassword(String password,String id){
		String sql="update user set user_password=? where user_id=?";
		SqlHelp.update(sql,password, id);
	}
	/*
	 * 注册
	 */
	public int userRegister(User user){
		//3)处理业务逻辑
		String sql="INSERT into user (user_id,user_name,user_password,user_telphone,user_email,user_sex,user_registertime,user_status,user_photo) VALUES (?,?,?,?,?,?,now(),1,?)";
		int i=SqlHelp.update(sql,user.getUserId(),user.getUserName(),user.getUserPassword(),user.getUserTelphone(),user.getUserEmail(),user.getUserSex(),user.getUserPhoto());
		return i;
	}
	/*
	 * 查看个人信息
	 */
	public User selectUserInformation(String userId){
		String sql="select * from user where user_id=?";
		 List<HashMap<String,Object>> list=SqlHelp.select3(sql,userId);
		 User user=null;
		 if(list.size()>0){
			 HashMap<String ,Object> map=list.get(0);
			 user=new User();
			 user.setUserId(map.get("user_id")==null?null:map.get("user_id").toString());
			 user.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
			 user.setUserTelphone(map.get("user_telphone")==null?null:map.get("user_telphone").toString());
			 user.setUserAddress(map.get("user_address")==null?null:map.get("user_address").toString());
			 user.setUserEmail(map.get("user_email")==null?null:map.get("user_email").toString());
			 user.setUserSex(map.get("user_sex")==null?null:map.get("user_sex").toString());
			 user.setUserAge(map.get("user_age")==null?null:map.get("user_age").toString());
			 user.setUserRegisterTime(map.get("user_registertime")==null?null:map.get("user_registertime").toString());
			 user.setUserpraise(map.get("user_praise")==null?null:map.get("user_praise").toString());
			 user.setUserbad(map.get("user_bad")==null?null:map.get("user_bad").toString());
			 user.setUserPhoto(map.get("user_photo")==null?null:map.get("user_photo").toString());
		 }
		 return user;
	}
	/**
	 * 修改头像
	 * @param userName
	 * @return
	 */
	
	public void headportraituser(String photo,String userid) {
		String sql="update user set user_photo=? where user_id=?";
		SqlHelp.update(sql, photo,userid);
	}

	
	
	/*
	 * 忘记密码
	 */
	public  List<HashMap<String,Object>> forgetPwd(String userName){
		String sql="select * from user where user_name=?";
		 List<HashMap<String,Object>> list=SqlHelp.select3(sql,userName);
		 return list;
		/* User user=null;
		 if(list.size()>0){
			 HashMap<String ,Object> map=list.get(0);
			 user=new User();
			 user.setUserId(map.get("user_id")==null?null:map.get("user_id").toString());
			 user.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
			 user.setUserTelphone(map.get("user_telphone")==null?null:map.get("user_telphone").toString());
			 user.setUserAddress(map.get("user_address")==null?null:map.get("user_address").toString());
			 user.setUserEmail(map.get("user_email")==null?null:map.get("user_email").toString());
			 user.setUserSex(map.get("user_sex")==null?null:map.get("user_sex").toString());
			 user.setUserAge(map.get("user_age")==null?null:map.get("user_age").toString());
			 user.setUserRegisterTime(map.get("user_registertime")==null?null:map.get("user_registertime").toString());
			 
		 }
		 return user;*/
		
	}
	/**
	 * 忘记密码2  短信验证
	 */
	public List<HashMap<String,Object>> forgetPwd2(String userName,String telphone){
		String sql="select * from user where user_name=? and user_telphone=?";
		List<HashMap<String,Object>> list=SqlHelp.select3(sql,userName,telphone);
		return list;
		/* User user=null;
		 if(list.size()>0){
			 HashMap<String ,Object> map=list.get(0);
			 user=new User();
			 user.setUserId(map.get("user_id")==null?null:map.get("user_id").toString());
			 user.setUserName(map.get("user_name")==null?null:map.get("user_name").toString());
			 user.setUserTelphone(map.get("user_telphone")==null?null:map.get("user_telphone").toString());
			 user.setUserAddress(map.get("user_address")==null?null:map.get("user_address").toString());
			 user.setUserEmail(map.get("user_email")==null?null:map.get("user_email").toString());
			 user.setUserSex(map.get("user_sex")==null?null:map.get("user_sex").toString());
			 user.setUserAge(map.get("user_age")==null?null:map.get("user_age").toString());
			 user.setUserRegisterTime(map.get("user_registertime")==null?null:map.get("user_registertime").toString());
			 
		 }
		 return user;*/
		
	}
	public int forgetPwd3(String password,String userId){
		String sql="update user set user_password=? where user_id=?";
		int i=SqlHelp.update(sql, password,userId);
		return i;
	}
	
	
	
}
