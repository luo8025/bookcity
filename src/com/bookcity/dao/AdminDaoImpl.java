package com.bookcity.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bookcity.common.PageModel;
import com.bookcity.model.Admin;
import com.bookcity.model.User;
import com.bookcity.unitl.SqlHelp;
import com.bookcity.unitl.StringUtil;




public class AdminDaoImpl {
       //查询所有用户信息
	 public PageModel<List> selectuser(String page,String pageSize) {
			String sql="select user_id,user_name,user_address,user_telphone,user_email,user_sex,user_age,user_photo,user_registertime,user_status,user_praise  from user ";
	    	PageModel pagemodel=new PageModel<>(sql,page,pageSize);
			
			List<HashMap<String,Object>> list=SqlHelp.select3(pagemodel.toMysqlSql());
			
			List<User> list2=new ArrayList<User>();
			if(list.size()>0) {
				for(HashMap<String,Object> map:list) {
					User user=new User();
					user.setUserId(StringUtil.valueof(map.get("user_id")));
		 			user.setUserName(StringUtil.valueof(map.get("user_name")));
		 			user.setUserAddress(StringUtil.valueof(map.get("user_address")));
		 			user.setUserTelphone(StringUtil.valueof(map.get("user_telphone")));
		 			user.setUserEmail(StringUtil.valueof(map.get("user_email")));
		 			user.setUserSex(StringUtil.valueof(map.get("user_sex")));
		 			user.setUserAge(StringUtil.valueof(map.get("user_age")));
		 			user.setUserPhoto(StringUtil.valueof(map.get("user_photo")));
		 			user.setUserRegisterTime(StringUtil.valueof(map.get("user_registertime")));
		 			user.setUserstatus(StringUtil.valueof(map.get("user_status")));
		 			user.setUserbantime(StringUtil.valueof(map.get("user_bantime")));
		 			user.setUserbanreason(StringUtil.valueof(map.get("user_banreason")));
		 			user.setUserpraise(StringUtil.valueof(map.get("user_praise")));
		 			list2.add(user);
				}
			}
	    	   
			pagemodel.setList(list2);
			List<HashMap<String,Object>> countlist=SqlHelp.select3(pagemodel.toCountSql());
			String total=StringUtil.valueof(countlist.get(0).get("count"));
			pagemodel.setTotal(Integer.valueOf(total));
	    	   
	    		return pagemodel;
	  } 
	 //按用户id封停账号
	 public void Titleuser(String userid) {
			String sql=" update user set user_status=0,user_bantime=NOW() where user_id=?";
	    	SqlHelp.update(sql,userid);
	  } 
	 //按用户id和用户状态解封账号
	 public void Unlockuser(String userid) {
		 String sql="update user set user_status=1,user_bantime=null,user_banreason=null where user_id=?";
		 SqlHelp.update(sql, userid);
	 }
	 //按用户id删除账号
	 public void Deleteuser(String userid) {
		 String sql="delete from user where user_id=?";
		 SqlHelp.update(sql, userid);
	 }
	 //管理员按id查询一个用户的信息
	 public User selectuserid(String userid) {
		 String sql="select * from user where user_id=?";
		 User user=null;
 		List<HashMap<String,Object>> list=SqlHelp.select3(sql, userid);
 		if(list.size()>0) {
 			HashMap<String,Object> map=list.get(0);
 			user =new User();
 			user.setUserId(StringUtil.valueof(map.get("user_id")));
 			user.setUserName(StringUtil.valueof(map.get("user_name")));
 			user.setUserAddress(StringUtil.valueof(map.get("user_address")));
 			user.setUserTelphone(StringUtil.valueof(map.get("user_telphone")));
 			user.setUserEmail(StringUtil.valueof(map.get("user_email")));
 			user.setUserSex(StringUtil.valueof(map.get("user_sex")));
 			user.setUserAge(StringUtil.valueof(map.get("user_age")));
 			user.setUserPhoto(StringUtil.valueof(map.get("user_photo")));
 			user.setUserRegisterTime(StringUtil.valueof(map.get("user_registertime")));
 			user.setUserstatus(StringUtil.valueof(map.get("user_status")));
 			user.setUserbantime(StringUtil.valueof(map.get("user_bantime")));
 			user.setUserbanreason(StringUtil.valueof(map.get("user_banreason")));
 			user.setUserpraise(StringUtil.valueof(map.get("user_praise")));
 		}
 	return user;	
	 }
	 //按天统计售卖金额
	 public List<HashMap<String,Object>> selectSales(){
			String sql="select order_time time,sum(order_total) sum FROM `order` GROUP BY order_time";
	    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
	    		return list;
		 
	 }
	 //按月统计售卖金额
	 public List<HashMap<String,Object>> selectSalesmonth(){
			String sql="SELECT  DATE_FORMAT(order_time, '%Y-%m') AS month , sum(order_total) sum FROM `order` GROUP BY month";
	    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
	    		return list;
		 
	 }
	 //按年统计售卖金额
	 public List<HashMap<String,Object>> selectSalesyear(){
			String sql="SELECT  DATE_FORMAT(order_time, '%Y') AS years , sum(order_total) sum FROM `order` GROUP BY years;";
	    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
	    		return list;
		 
	 }
	//按种类统计数量
		 public List<HashMap<String,Object>> selectcategorycount(){
				String sql="SELECT book_category  category,count(book_category) count  FROM book GROUP BY book_category";
		    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
		    		return list;
			 
		 }
		//按图书售卖城市统计数量
		 public List<HashMap<String,Object>> selectbookcity(){
				String sql="SELECT  book_city city, count(book_city) count from `order` LEFT JOIN book on order_book_id=book_id GROUP BY book_city ";
		    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
		    		return list;
			 
		 }
		//统计用户所在城市
		 public List<HashMap<String,Object>> selectusercity(){
				String sql="SELECT  user_address  city ,count(user_address) count from `user` GROUP BY user_address";
		    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
		    		return list;
			 
		 }
		 //管理员登陆
		 public Admin loginAdmin(String adminname ,String password){
				//3)处理业务逻辑
				String sql="select * from admin where admin_name=? and admin_password=? ";
				 List<HashMap<String,Object>> list=SqlHelp.select3(sql,adminname,password);
				 Admin admin=null;
				 if(list.size()>0){
					 HashMap<String ,Object> map=list.get(0);
					 admin=new Admin();
					 admin.setAdminId(map.get("admin_id")==null?null:map.get("admin_id").toString());
					 admin.setAdminName(map.get("admin_name")==null?null:map.get("admin_name").toString());
					 admin.setAdminPassword(map.get("admin_password")==null?null:map.get("admin_password").toString());
					 admin.setAdmiTelphone(map.get("admin_telphone")==null?null:map.get("admin_telphone").toString());
					 admin.setAdminEmail(map.get("admin_email")==null?null:map.get("admin_email").toString());
					 admin.setAdminSex(map.get("admin_sex")==null?null:map.get("admin_sex").toString());
					 admin.setAdminAge(map.get("admin_age")==null?null:map.get("admin_age").toString());
				 }
				 return admin;
			}
		 //管理员列表
		 public  List<HashMap<String,Object>> selectalladmin(){
			  String sql="SELECT * from admin";
			  List<HashMap<String,Object>> list=SqlHelp.select3(sql);
			  return list;
		  }
		 
		//统计加入购物车种类
		 public List<HashMap<String,Object>> selectshopactcount(){
				String sql="SELECT book_category category,count(book_category) count FROM shopcart LEFT JOIN book on shopcart_book_id=book_id  GROUP BY book_category  ";
		    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
		    		return list;
			 
		 }
		//统计加入订单种类
		 public List<HashMap<String,Object>> selectordercount(){
				String sql="SELECT book_category category,count(book_category) count FROM `order` LEFT JOIN book on order_book_id=book_id  GROUP BY book_category   ";
		    	   List<HashMap<String,Object>> list=SqlHelp.select3(sql);
		    		return list;
			 
		 }
	 
	
	
}
