package com.bookcity.dao;

import java.util.HashMap;
import java.util.List;

import com.bookcity.model.Order;
import com.bookcity.unitl.SqlHelp;



public class OrderDaoImpl {
	
	
	
	/*
	 *  生成订单（插入订单）结算
	 */
	public void settlement(Order o){
		String sql="INSERT INTO `order` VALUES(?,?,?,NOW(),1,?,?,?)";
		SqlHelp.update(sql,o.getOrderid(),o.getOrderUserId(),o.getOrderUserName(),o.getOrderTotal(),o.getOrderSendPlace(),o.getOrderBookId());
	}
	/**
	 * 更新购物车的ODER_ID
	 */
	public void updateCar(String shopcard_order_id,String shopcart_user_id,String shopcart_id){
		String sql="UPDATE shopcart set shopcart_order_id=? where shopcart_user_id=? and shopcart_id=?";
		SqlHelp.update(sql, shopcard_order_id,shopcart_user_id,shopcart_id);
	}

	
	
	
	
	//用户购买订单查询
	public List<HashMap<String, Object>> selectbuy(String orderUserId) {
		String sql="select* FROM `order` WHERE order_user_id=? AND order_is_valid='1'  ORDER BY order_time  DESC";                //1购买
		List<HashMap<String ,Object>> list=SqlHelp.select3(sql, orderUserId);
		return list;
	}
	//用户出售订单查询
	public List<HashMap<String, Object>> selectsell(String orderUserId) {
		String sql="select* FROM `order` WHERE order_user_id=? AND order_is_valid='1'  ORDER BY order_time  DESC";                //0出售
		List<HashMap<String ,Object>> list=SqlHelp.select3(sql, orderUserId);
		return list;
	}
	//取消订单
	public void  cancelorder(String orderId,String OrderUserId) {
		String sql="UPDATE `order` SET order_is_valid=0 WHERE  order_id=? AND order_user_id=?";
		SqlHelp.update(sql, orderId,OrderUserId);
	}
	//管理员删除订单
	public void  deleteorder(String orderId) {
		String sql="delete from `order` where order_id=?";
		SqlHelp.update(sql, orderId);
	}
	 //订单列表
	 public  List<HashMap<String,Object>> selectAllorder(){
		  String sql="SELECT * from `order`where order_is_valid=0  ORDER BY order_time DESC";
		  List<HashMap<String,Object>> list=SqlHelp.select3(sql);
		  return list;
	  }
	 /*
	  * 插入地址
	  */
	 public int insertAddress(String address,String id){
		 String sql="update order set order_sendplace=? where order_id=?";
		 
		 int i=SqlHelp.update(sql, address,id);
		 return i;
	 }
	

}
