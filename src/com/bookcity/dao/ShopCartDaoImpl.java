package com.bookcity.dao;

import java.util.HashMap;
import java.util.List;

import com.bookcity.model.ShopCart;
import com.bookcity.unitl.SqlHelp;

public class ShopCartDaoImpl {
	/*
	 * 加入购物车
	 */
	public int addToCart(ShopCart sc){
		String sql="insert into ShopCart values(?,?,?,?,?,?,?,?,?,now())";
		int i=SqlHelp.update(sql, sc.getShopcart_id(),sc.getShopcart_book_id(),sc.getShopcart_order_id(),sc.getShopcart_user_id(),sc.getShopcart_book_name(),sc.getShopcart_book_price(),sc.getShopcart_number(),sc.getShopcart_subtotal(),sc.getShopcart_total());
		return i;
	}
	/*
	 * 查看购物车
	 */
	public List<HashMap<String,Object>> selectProductByCar(String id){
		String sql="SELECT sc.*,b.book_id,b.book_photo,b.book_author,b.book_remarks,b.book_stock,b.book_damage,b.book_user_id,u.user_name FROM shopcart sc LEFT JOIN book b ON sc.shopcart_book_id = b.book_id  LEFT JOIN `user` u on b.book_user_id=u.user_id WHERE sc.shopcart_user_id =?  AND sc.shopcart_order_id='' AND b.book_stock > 0 ORDER BY	sc.shopcart_creattime ASC";
		List<HashMap<String,Object>> list=SqlHelp.select3(sql,id);
		return list;
	}
	/**
	 * 移除购物车
	 */
	public void deleteCarShopById(String id){
		//3)处理业务逻辑
		String sql="DELETE from shopcart where shopcart_id=?";
		SqlHelp.update(sql,id);
	}
}
