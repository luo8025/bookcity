package com.bookcity.common;

import java.io.IOException;

import javax.servlet.ServletResponse;

import com.google.gson.Gson;

public class JsonResultWriter {
	
	public static void writer( ServletResponse response,Object object) throws IOException{
		Gson gson=new Gson();
		String json=gson.toJson(object);
		//4)响应数据的编码和类型
		response.setContentType("application/json");//设置响应类型
		//5)获得json数据
		response.getWriter().println(json);
		response.getWriter().close();
	}
	
	
}
